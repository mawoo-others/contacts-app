import React, { Component } from "react";
import PropTypes from "prop-types";

// Style
import "./ContactItem.css";

class ContactItem extends Component {
  render() {
    let searchText = this.props.searchText;
    let pattern = new RegExp(searchText, "gi");
    let contact;

    if (searchText.length >= 1) {
      contact = {
        name: highlightSearchResults(this.props.contact.name),
        username: highlightSearchResults(this.props.contact.username),
        phone: this.props.contact.phone,
        email: this.props.contact.email,
        website: this.props.contact.website
      };
    } else {
      contact = this.props.contact;
    }

    function highlightSearchResults(property) {
      let highlightedSearchResults = property.replace(pattern, function(match) {
        return (
          '<span class="contact-list__item-highlight">' + match + "</span>"
        );
      });

      return highlightedSearchResults;
    }

    return (
      <li className="contact-list__item">
        <h3 dangerouslySetInnerHTML={{ __html: contact.name }} />
        <ul>
          <li
            dangerouslySetInnerHTML={{
              __html: "Użytkownik: " + contact.username
            }}
          />
          <li>
            Telefon: <a href={"tel:" + contact.phone}>{contact.phone}</a>
          </li>
          <li>
            E-mail: <a href={"mailto:" + contact.email}>{contact.email}</a>
          </li>
          <li>
            Strona WWW:{" "}
            <a href={"http://" + contact.website}>{contact.website}</a>
          </li>
        </ul>
      </li>
    );
  }
}

ContactItem.propTypes = {
  contact: PropTypes.object,
  searchText: PropTypes.string
};

export default ContactItem;
