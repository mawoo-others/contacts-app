import React, { Component } from "react";
import PropTypes from "prop-types";

// Style
import "./ContactList.css";

// Components
import ContactItem from "../ContactItem/ContactItem";

class ContactList extends Component {
  render() {
    let filteredContacts;
    let searchText = this.props.searchText;
    let pattern = new RegExp(searchText, "gi");
    let alert;

    if (searchText.length >= 1) {
      filteredContacts = this.props.contacts.filter(element => {
        return pattern.test(element.name) || pattern.test(element.username);
      });
    } else {
      filteredContacts = this.props.contacts;
    }

    if (filteredContacts.length === 0) {
      alert = <div className="alert">Kontaktu nie znaleziono :(</div>;
    }

    return (
      <div>
        {alert}
        <ul className="contact-list">
          {filteredContacts.map((contact, index) => {
            return (
              <ContactItem
                key={index}
                contact={contact}
                searchText={this.props.searchText}
              />
            );
          })}
        </ul>
      </div>
    );
  }
}

ContactList.propTypes = {
  contacts: PropTypes.arrayOf(PropTypes.object),
  searchText: PropTypes.string
};

export default ContactList;
