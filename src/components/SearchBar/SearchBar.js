import React, { Component } from "react";
import PropTypes from "prop-types";

import "./SearchBar.css";

class SearchBar extends Component {
  updateSearchText(event) {
    this.props.updateSearchText(event.target.value);
  }

  render() {
    return (
      <div className="search-bar">
        <label htmlFor="search">Wyszukaj kontakt</label>
        <input
          id="search"
          maxLength="16"
          name="search"
          onChange={this.updateSearchText.bind(this)}
          placeholder="Min. 3 litery"
          type="search"
        />
      </div>
    );
  }
}

SearchBar.propTypes = {
  updateSearchText: PropTypes.func
};

export default SearchBar;
