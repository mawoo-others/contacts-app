import React, { Component } from "react";

// Style
import "./Preloader.css";

// Images
import preloaderImage from "./preloader.svg";

class Preloader extends Component {
  render() {
    return (
      <div className="Preloader">
        <img
          className="Preloader__image"
          src={preloaderImage}
          alt="Preloader"
        />
      </div>
    );
  }
}

export default Preloader;
