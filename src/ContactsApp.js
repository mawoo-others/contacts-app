import React, { Component } from "react";
import axios from "axios";

// Style
import "./ContactsApp.css";

// Components
import Preloader from "./components/Preloader/Preloader";
import SearchBar from "./components/SearchBar/SearchBar";
import ContactList from "./components/ContactList/ContactList";

class ContactsApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      preloader: true,
      searchText: ""
    };
  }

  componentDidMount() {
    this.getContactsList();
  }

  getContactsList() {
    this.sendRequestToRESTAPI("https://jsonplaceholder.typicode.com/users");
  }

  sendRequestToRESTAPI(url) {
    const reference = this;

    axios
      .get(url, {
        params: {}
      })
      .then(function(response) {
        const newState = Object.assign({}, reference.state);

        newState.contacts = response.data;
        newState.preloader = false;

        reference.setState(newState);
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  updateSearchText(text) {
    const newState = Object.assign({}, this.state);

    newState.searchText = text;
    this.setState(newState);
  }

  render() {
    let preloader, searchBar, contactList;

    if (this.state.preloader) {
      preloader = <Preloader />;
    } else {
      searchBar = (
        <SearchBar updateSearchText={this.updateSearchText.bind(this)} />
      );
      contactList = (
        <ContactList
          contacts={this.state.contacts}
          searchText={this.state.searchText}
        />
      );
    }

    return (
      <section className="Contacts">
        <h2>Kontakty</h2>
        {preloader}
        {searchBar}
        {contactList}
      </section>
    );
  }
}

export default ContactsApp;
